﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextController : MonoBehaviour {

    public UnityEngine.UI.Text text;

	// Use this for initialization
	void Start () {
        text.text = "Hello World";
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) {
            text.text = "Space key pressed";
        }
		
	}
}
